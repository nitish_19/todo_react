export type Todo = {
    task: string;
    active: boolean;
};

export type ActiveStatus = (selectedTodo: Todo) => void;

export type AddTodo = (newTodo: string) => void;
