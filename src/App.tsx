import React, { FunctionComponent, useEffect, useState } from 'react';
import { TodoList } from './components/TodoList';
import { CreateTodo } from './components/CreateTodo';
import { Todo, ActiveStatus, AddTodo } from './API'
import './App.css';

export const App: FunctionComponent = () => {
  
  let [ todos, setState ] = useState< Array<Todo> >([]);

  const activeStatus: ActiveStatus = selectedTodo => {
    const updatedTodos = todos.filter( (todo: Todo, index: number) => todo!==selectedTodo);
    setState(updatedTodos);
  };

  const addTodo: AddTodo = newTodo => {
    if(newTodo.trim()!=="") { 
      setState([...todos, {task: newTodo, active: false}]);
    }
  };
  useEffect(() => {
   todos = JSON.parse(localStorage.getItem('todolist') || '[]');
   if(todos){
     setState(todos);
   }
 }, []);

  useEffect(() => {
    localStorage.setItem('todolist', JSON.stringify(todos));
  }, [todos]);
   
  return (
    <React.Fragment>
      <CreateTodo addTodo={ addTodo } />
      <TodoList todos={todos} activeStatus = { activeStatus }  />
    </React.Fragment>
  );
}

export default App;
