import React, { FunctionComponent, useState, ChangeEvent, FormEvent } from "react";
import { AddTodo } from "../API";

interface CreateTodoProps {
  addTodo: AddTodo;
}

export const CreateTodo: FunctionComponent< CreateTodoProps > = ({ addTodo }) => {
  const [ newTodo, setState ] = useState< string >("");

  const handleChange = (e: ChangeEvent< HTMLInputElement >) => {
    setState( e.target.value );
  };

  const handleSubmit = (e: FormEvent< HTMLButtonElement >) => {
    e.preventDefault();
    addTodo( newTodo );
    setState("");
  };
  
  return (
    <form className = "container-form">
      <input type="text" value={newTodo} onChange={ handleChange } placeholder = "Add task"/>{'   '}
      <button type="submit" onClick={ handleSubmit } >
        Add Todo
      </button>
    </form>
  );
};