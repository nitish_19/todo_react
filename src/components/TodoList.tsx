import React, { FunctionComponent }from "react";
import { Todo, ActiveStatus } from "../API";
import { TodoListItem } from "./TodoListItem";
import './../TodoListItem.css';

interface TodoListProps {
  todos: Array< Todo >;
  activeStatus: ActiveStatus
}

export const TodoList: FunctionComponent< TodoListProps > = ({
  todos,
  activeStatus,
}) => {

  return (
    <ol className = "container-list">
      {todos.map((todo) => (
        <TodoListItem
          todo={todo}
          activeStatus={activeStatus}
        />
      ))}
    </ol>
  );
};