import React, { FunctionComponent, useState, ChangeEvent } from "react";
import "./../TodoListItem.css";
import { Todo, ActiveStatus } from '../API';

interface TodoListItemProps {
  todo: Todo;
  activeStatus: ActiveStatus;
}
 
export const TodoListItem: FunctionComponent< TodoListItemProps > = ({
  todo,
  activeStatus,
}) => {

  const [ editTodoTask, setState ] = useState< string >( todo.task );
  const [ editedTodo, setTodo ] = useState< Todo > ( todo );

  const handleChange = (e: ChangeEvent< HTMLTextAreaElement >) => {
    setState( e.target.value );
    setTodo({task: editTodoTask, active: false} );
  };
   
  return (
    <li className={ "active" } >
      <label >
        <button
          type="submit"
          onClick = {() => activeStatus(editedTodo)}
        >Mark ✅ </button>{'  '}
      </label> 
      {'  '}
      {editTodoTask} <br/>
      <textarea rows={2} cols={90} value = {editTodoTask} onChange = {handleChange} /> <hr/>
    </li>
  );
};